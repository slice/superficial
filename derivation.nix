{
  pkgs ? import <nixpkgs> {}
}:

pkgs.python3Packages.buildPythonApplication {
  pname = "superficial";
  src = ./.;
  version = "0.0";
  propagatedBuildInputs = with pkgs.python3Packages; [ requests ];
}
