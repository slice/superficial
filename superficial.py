#!/usr/bin/env python3

import argparse
import base64
from pathlib import Path

import requests


def run(*, webhook_url):
    session = requests.Session()
    session.headers.update(
        {
            "User-Agent": "superficial/0.0",
        }
    )

    url = "https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=handshake"
    resp = session.get(url)
    resp.raise_for_status()

    resp = resp.json()

    if len(resp) == 0:
        raise RuntimeError("response was empty")
    hns = resp[0]
    current_price = hns["current_price"]
    day_delta = hns["price_change_24h"]

    balance = 4_246.89
    usd = balance * current_price

    if day_delta < 0:
        day_delta_emoji = "\N{CHART WITH DOWNWARDS TREND}"
    else:
        day_delta_emoji = "\N{CHART WITH UPWARDS TREND}"

    resp = session.post(
        webhook_url,
        json={
            "content": f"${usd:.2f} (${current_price:.4f}, {day_delta_emoji} ${day_delta})"
        },
    )
    resp.raise_for_status()


if __name__ == "__main__":
    p = argparse.ArgumentParser(description="hns airdrop monitor")
    p.add_argument(
        "-w",
        "--webhook-url",
        required=True,
        help="the discord webhook url to post to",
        metavar="URL",
    )
    args = p.parse_args()

    run(webhook_url=args.webhook_url)
