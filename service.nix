{ config, lib, pkgs, ... }:

with lib;

let
  pkg = pkgs.callPackage ./derivation.nix {};
  cfg = config.services.superficial;
in {
  options.services.superficial = {
    enable = mkEnableOption "superficial";
    webhookUrl = mkOption {
      type = types.str;
      default = "";
      example = "";
    };
    timer = mkOption {
      type = types.str;
      default = "daily";
      example = "";
    };
  };

  config.systemd = mkIf cfg.enable {
    timers.superficial = {
      wantedBy = [ "timers.target" ];
      partOf = [ "superficial.service" ];
      timerConfig.OnCalendar = cfg.timer;
    };
    services.superficial = {
      serviceConfig.Type = "oneshot";
      after = [ "network-online.target" ];
      wantedBy = [ "network-online.target" ];
      script = "${pkg}/bin/superficial.py --webhook-url ${escapeShellArg cfg.webhookUrl}";
    };
  };
}
