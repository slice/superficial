#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='superficial',
    version='0.0',
    scripts=['superficial.py'],
)
